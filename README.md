# Pahlawan

# Overview

This is a product site with a separate back-end and front-end.

# Get Started

## Pre-Requisites

Run the commands from the following 3 sections before beginning development.

### Global Dependencies

Install the global dependency `strapi@beta`:

```sh
npm install --global strapi@v3.0.0-beta.15;
```

### Local Dependencies

Install the local dependencies via:

```sh
make deps;
```

Alternatively, navigate to both `./src/www` and `./src/api` and run `yarn`.

### Support Services

To start a compatible MySQL database instance, run:

```sh
make start_db;
```

A MySQL database should be available on your host machine at port 3306.

## Application Quick Start

To start the API:

```sh
make start_api
```

To start the website:

```sh
make start_www
```

# Runbooks

See [`./src/www/README.md`](./src/www/README.md) for the web front-end documentation.

See [`./src/api/README.md`](./src/api/README.md) for the application programming interface (API) documentation.
