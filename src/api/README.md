# API

The API is based on [Strapi](https://strapi.io).

# Running in...

## Development

Start the API using:

```sh
yarn develop
```

The admin dashboard should be available at http://localhost:1337/admin

## Production

Start the API using:

```sh
yarn start
```

# Configuration

## Environment Variables

| Key | Description | Example |
| --- | --- | --- |
| `DATABASE_HOST` | Hostname of the database server | `"127.0.0.1"` |
| `DATABASE_PORT` | Port the database server is listening on | `3306` |
| `DATABASE_SRV` | | `false` |
| `DATABASE_NAME` | Name of the schema to use in the database | `"database"` |
| `DATABASE_USERNAME` | Username to login to the database | `"user"` |
| `DATABASE_PASSWORD` | Password to login to the database | `"password"` |
| `DATABASE_SSL` | Indiciates whether to use SSL | `false` |
| `DATABASE_AUTHENTICATION_DATABASE` | Don't really know | `""` |

# Building/Packaging

Navigate to the root directory and run `make build_api`.

# Content Types Overview

## Page

## Company

## Product

## Category
