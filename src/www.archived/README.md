# UI

The UI is based on [Next.js](https://nextjs.org/).

# Running in...

## Development

Start the UI using:

```sh
yarn develop;
```

The site should be available at http://localhost:3000

## Production

Build the static site using:

```sh
yarn export;
```

A static HTML site will be available at `./out`. Serve it using your favourite web server!

# Building/Packaging

Navigate to the root directory and run `make build_www`.
