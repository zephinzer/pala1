import showdown from 'showdown';

export function getMarkdownParser(...withExtensions) {
  const markdownConverter = new showdown.Converter();
  if (withExtensions instanceof Array) {
    withExtensions.forEach((extension) => {
      markdownConverter.addExtension(extension);
    });
  }
  return markdownConverter;
}

export function getAbsoluteURLResolver(hostname, targetStartPath) {
  return function() {
    return {
      type: 'output', //or output
      regex: new RegExp(`${targetStartPath}`), // if filter is present, both regex and replace properties will be ignored
      replace: `${hostname}/uploads`,
    };
  }
};

export function getContentWrapperInserter(className) {
  return function() {
    return {
      type: 'output', //or output
      filter: (text) => {
        return `<span class="${className}">${text}</span>`;
      },
    };
  }
}