import React from 'react';
import Head from 'next/head';
import {withRouter} from 'next/router';
import fetch from 'isomorphic-unfetch';

import Page from '../../components/Page';
import Layout from '../../components/Layout';
import {getMarkdownParser} from '../../utilities/markdown-parser';

class SluggedPage extends React.Component {
  constructor() {
    super();
    this.state = {
      data: {},
    }
    return this;
  }

  static async getInitialProps({query}) {
    console.error('get Initial Props')
    const initialProps = {
      apiURL: process.env.NEXT_PUBLIC_API_SERVER_URL,
      data: {},
    };
    const {slug} = query;
    const response = await fetch(`${initialProps.apiURL}/pages?slug=${slug}`);
    const jsonResponse = await response.json();
    initialProps.data = jsonResponse.length > 0 ? jsonResponse[0] : {};
    return initialProps
  }

  render () {
    const markdownConverter = getMarkdownParser();
    const {data} = this.props;
    const content = markdownConverter.makeHtml(data.content);
    const splashImageURL = data.header_image && data.header_image.url ?
      `${process.env.NEXT_PUBLIC_API_SERVER_URL}${data.header_image.url}`
      : '/static/img/ruralhobiton/ruralhobiton.jpg';
    return (
      <Layout className="slugged-page">
        <Head>
          <title>{data.title}</title>
        </Head>

        <Page
          title={data.title}
          content={content}
          splashImageURL={splashImageURL}
        />
      </Layout>
    );
  }
};

export default withRouter(SluggedPage);
