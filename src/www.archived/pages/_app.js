import React from 'react'
import App from 'next/app'
import {library} from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons';
import {Provider} from 'react-redux';
import withReduxStore from '../components/Store';

import 'bootstrap-css-only/css/bootstrap.min.css';
import '../static/global.css';

library.add(fas);

class Main extends App {
  render() {
    console.info('main rendered');
    const {Component, pageProps, reduxStore} = this.props;
    return (
      <Provider store={reduxStore}>
        <Component {...pageProps} />
      </Provider>
    );
  }
}

export default withReduxStore(Main);
