const withPlugins = require('next-compose-plugins');
const nextEnv = require('next-env');
const dotenvLoad = require('dotenv-load');
const withCSS = require('@zeit/next-css');
const withFonts = require('next-fonts');

const plugins = [
  [withCSS],
  [withFonts],
];

if (process.env.NODE_ENV === 'development') {
  dotenvLoad();
  plugins.push([nextEnv()]);
}

module.exports = withPlugins(plugins, {});
