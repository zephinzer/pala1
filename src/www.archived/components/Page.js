import React from 'react';

const Page = ({
  content,
  splashImageURL,
  title,
}) => (
  <div className="page">
    <div className="splash-container">
      <div className="splash-image">
        
      </div>
    </div>

    <div className="title-container">
      <h1 className="title">
        {title}
      </h1>
    </div>

    <div className="content-container">
      <div className="content" dangerouslySetInnerHTML={{__html: content}} />
    </div>

    <style jsx>{`
      .page {
        text-align: center;
      }
      .content-container {
        text-align: center;
        width: 100%;
      }
      .content {
        background-color: rgba(255, 255, 255, 0.9);
        border-top-left-radius: 32px;
        box-shadow: 8px 8px 32px rgba(0, 0, 0, 0.2);
        display: inline-block;
        font-size: 20px;
        text-align: left;
        padding: 32px 32px;
        max-width: 1100px;
        width: 100%;
      }
      .splash-container {
        box-shadow: 0px 0px 64px rgba(0, 0, 0, 0.6);
        height: 666px;
        margin-bottom: -256px;
        text-align: center;
        width: 100%;
      }
      .splash-image {
        align-items: flex-end;
        background-image: url(${splashImageURL});
        background-size: cover;
        background-position: center;
        display: flex;
        flex-direction: row;
        height: 100%;
        margin: 0;
        width: 100%;
      }
      .title-container {
        display: inline-block;
        margin-bottom: 64px;
        max-width: 1252px;
        text-align: left;
        width: 100%;
      }
      .title {
        background-color: rgba(255, 255, 255, 0.8);
        box-shadow: 8px 8px 32px rgba(0, 0, 0, 0.5);
        border-bottom-right-radius: 32px;
        display: inline;
        font-size: 80px;
        font-weight: 800;
        margin: 16px 32px;
        margin-bottom: 64px;
        padding: 0px 16px;
        text-outline: 1px #FFF;
        text-shadow: 0px 0px 5px rgba(255, 255, 255, 1);
      }
    `}</style>
  </div>
);

export default Page;
