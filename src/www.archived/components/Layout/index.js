import React from 'react';
import Nav from '../Nav';

export default class Layout extends React.Component {
  render() {
    console.info('layout rendered');
    return (
      <div className={`components-layout-container ${this.props.containerClassName || ''}`}>
        <Nav />
        <div className={`components-layout ${this.props.className || ''}`}>
          {this.props.children}
        </div>
        <style jsx>{`
          .components-layout-container {
            background-color: transparent;
            padding-bottom: 64px;
            text-align: center;
            width: 100%;
          }
          .components-layout-container .components-layout {
            display: inline-block;
            width: 100%;
          }
        `}</style>
      </div>
    )
  }
};
