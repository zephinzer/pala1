import React from 'react';
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';

const INITIAL_STATE = {
  pages: [],
};

const isServer = typeof window === 'undefined';
const __NEXT_REDUX_STORE__ = '__NEXT_REDUX_STORE__';

export default (App) => {
  return class AppWithRedux extends React.Component {
    static async getInitialProps(appContext) {
      const reduxStore = getOrCreateStore()
      appContext.ctx.reduxStore = reduxStore;
      let props = {};
      if (typeof App.getInitialProps === 'function') {
        props = {}; // await App.getInitialProps(appContext);
      }
      return {
        ...props,
        initialReduxState: reduxStore.getState(),
      };
    }

    constructor(props) {
      super(props);
      this.reduxStore = getOrCreateStore(props.initialReduxState);
    }

    render() {
      return (
        <App {...this.props} reduxStore={this.reduxStore} />
      );
    }
  };
};

function getOrCreateStore(initialState) {
  if (isServer) {
    return initializeStore(initialState);
  }
  if (!window[__NEXT_REDUX_STORE__]) {
    window[__NEXT_REDUX_STORE__] = initializeStore(initialState);
  }
  return window[__NEXT_REDUX_STORE__];
}

export const getPages = () => ({type: 'get_pages'});

const reducer = async (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case 'get_pages':
      console.info('get_pages');
      const response = await fetch(`${apiURL}/pages?_sort=order:asc`);
      const pages = response.json();
      return {
        ...state,
        pages: pages.map((page) => ({
          icon: page.fa_icon_id ? page.fa_icon_id : 'info',
          key: `page-${page.id}-${page.slug}`,
          title: page.title,
          url: `/page/${page.slug}`,
        })),
      };
    default:
      return state;
  }
};

function initializeStore(initialState = INITIAL_STATE) {
  return createStore(
    reducer,
    initialState,
    composeWithDevTools(applyMiddleware()),
  );
}
