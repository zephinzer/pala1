import React from 'react'
import Link from 'next/link'
import fetch from 'isomorphic-unfetch';
import Button from 'react-bootstrap/Button';
import Navbar from 'react-bootstrap/Navbar';
import NavBS from 'react-bootstrap/Nav';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {connect} from 'react-redux';
import {getPages} from '../Store';

class Nav extends React.Component {
  constructor() {
    super();
    this.state = {
      pages: [],
    };
    return this;
  }

  async componentDidMount() {
    console.info(this.props);
    const apiURL = process.env.NEXT_PUBLIC_API_SERVER_URL;
    const response = await fetch(`${apiURL}/pages?_sort=order:asc`);
    const body = await response.json();
    const pages = body.map((page) => ({
      icon: page.fa_icon_id ? page.fa_icon_id : 'info',
      key: `page-${page.id}-${page.slug}`,
      title: page.title,
      url: `/page/${page.slug}`,
    }))
    pages.unshift({
      icon: 'home',
      key: 'page-0-home',
      title: 'Home',
      url: '/',
    });
    this.setState({pages});
  }

  render() {
    return (
      <div className={`nav ${this.props.className || ''}`}>
        <Navbar className="bg-light" style={{
          width: '100%',
        }}>
          <Navbar.Brand
            style={{
              textAlign: 'left',
            }}
          >
            Pahlawan
          </Navbar.Brand>
          <div style={{
            flexGrow: 100,
          }}></div>
          <NavBS
            style={{
              flexGrow: 1,
            }}
          >
            {this.state.pages.map((page) => (
              <NavBS.Item key={page.key}>
                <Link href={page.url} as={page.url}>
                  <Button variant="link">
                    <FontAwesomeIcon size="1x" icon={['fas', `${page.icon}`]} />
                    <span style={{
                      display: 'block',
                      fontSize: '0.8em',
                    }}>
                      {page.title.toUpperCase()}
                    </span>
                  </Button>
                </Link>
              </NavBS.Item>
            ))}
          </NavBS>
        </Navbar>
        <style jsx>{`
          .nav {
            position: fixed;
            width: 100%;
          }
          .company-logo {
            color: red !important;
          }
        `}</style>
      </div>
    )
  }
};

export default connect(
  (state) => {
    return {
      state,
    };
  },
  {
    getPages,
  },
)(Nav);
