start_db:
	docker-compose -f ./deployments/db/docker-compose.yml up -d

start_api:
	cd ./src/api && yarn develop

start_www:
	cd ./src/www && yarn develop

start:
	docker-compose -f ./deployments/db/docker-compose.yml up -d
	docker-compose -f ./deployments/apps/docker-compose.yml up -d

status:
	docker-compose -f ./deployments/db/docker-compose.yml ps
	docker-compose -f ./deployments/apps/docker-compose.yml ps

deps:
	cd ./src/api && yarn
	cd ./src/www && yarn

logs:
	docker-compose -f ./deployments/docker-compose.yml logs -f

stop:
	docker-compose -f ./deployments/docker-compose.yml stop

purge:
	docker-compose -f ./deployments/docker-compose.yml rm

build_api:
	docker build -f ./build/api/Dockerfile -t pahlawan/api:latest .

build_www:
	docker build -f ./build/www/Dockerfile -t pahlawan/www:latest .
